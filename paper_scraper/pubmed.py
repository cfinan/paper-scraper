"""Utilities for interacting with pubmed. These are built on top of the
Biopython Entrez suite.
"""
# Importing the version number (in __init__.py) and the package name
from paper_scraper import (
    __version__,
    __name__ as pkg_name,
    log,
    cache as pubmed_cache
)

# Imports the Entries modules to query pubmed
from Bio import Entrez, Medline
from tqdm import tqdm
import time
import argparse
import sys
import os
import textwrap
import csv
import re
import pprint as pp


_SEARCH_SCRIPT_NAME = "pubmed-search"
"""The name for the pubmed text search program. (`str`)
"""
_PMID_SCRIPT_NAME = "pubmed-id-query"
"""The name for the pubmed ID search program. (`str`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedQuery(object):
    """Query pubmed either on search text or pubmed IDs.

    Parameters
    ----------
    email : `str`
        The e-mail address of the person doing the search. This is so pubmed
        can contact people doing excessive searches.
    cache : `str` or `NoneType`
        The path to a Python sheve file that will serve as the cache for query
        results. If it does not exist it will be created. Note, that the cache
        does not stop the query being performed but will be used instead of
        downloading citations and elinks. It is used if the query is identical
        and gives the same number of results and was issued within the
        ``cache_seconds_cutoff``. If ``NoneType``, then no cache will be used.
    cache_seconds_cutoff : `int`
        The time in seconds that cache qeures must be within in order to be
        considered for use.
    """
    _ELINK_MAX = 500
    """The number of PMIDs that are posted for an eLink query. If this is set
    too high then queries fail for some reason (`int`)
    """
    _MEDLINE_MAX = 1000
    """The number of medline entries that are retreived for an eSearch query.
    (`int`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, email, cache=pubmed_cache.CACHE_PATH,
                 cache_seconds_cutoff=7 * pubmed_cache.DAY):
        # Check the email is defined
        if email is None:
            raise ValueError("No E-mail provided!")

        # Get te cache to use. This will return a DummyCache object if the
        # cache is NoneType
        self.cache = pubmed_cache.get_cache(
            cache, seconds_cutoff=cache_seconds_cutoff
        )

        # Store the E-mail, also make sure it is assigned to Entrez so the NLM
        # know about it
        self.email = email
        Entrez.email = email

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def check_pubmed_id(pmid):
        """A method for checking that the pubmed ids look ok.

        At present to pass the test the ID must be an integer digit between 7-8
        digits long.

        Parameters
        ----------
        pmid : `str`
            A pubmed ID to test.

        Returns
        -------
        pmid : `str`
            The validated PMID (same as input)

        Raises
        ------
        ValueError
            If the pubmed ID is bad.
        """
        # At the moment I have set a min/max length for the PMIDs
        pubmed_min_length = 7
        pubmed_max_length = 8

        # Cast to a string
        pmid = str(pmid)

        # Test with a REGEXP and if OK then add to the passed list if not
        # the add to the failed list
        if not re.search(r'^\d{%i,%i}$' % (pubmed_min_length,
                                           pubmed_max_length), pmid):
            raise ValueError("bad pubmed ID: {pmid}")
        return pmid

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_text(self, search_text, relday=0, elink=False, verbose=False):
        """Conduct a text search and download all the medline records.

        Parameters
        ----------
        search_text : `str`
            The text search string.
        relday : `int`
            Relday is the timeframe that the search will look in so a relday
            of 365 is get the hits from the last year a relday of 0 just gets
            all hits irrespective of time frame.
        elink : `bool`, optional, default: `False`
            Download the e-link journal URLs as well as the search results.
        verbose : `bool`, optional, default: `False`
            Display a pubmed query progress bar for downloading citations and
            if ``elink`` is ``True``, for downloading eLinks.

        Returns
        -------
        search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
            The search summary object.
        medline_data : `list` of `paper_scraper.pubmed.MedlineRecord`
            The result medline entries.
        error_pmids : `dict`
            A log of errors associated with entries, the keys are PMIDs the
            values are the errors.

        Raises
        ------
        IndexError
            If there are no search results.
        """
        searched = self._do_text_search(search_text, relday=relday)

        medline_data = list()
        error_pmids = dict()
        count = searched.get_count()

        kwargs = dict(
            desc="[info] downloading citations",
            unit=" citations",
            disable=not verbose,
            total=count,
            leave=False
        )
        pbar = tqdm(**kwargs)
        try:
            for start in range(0, count, self._MEDLINE_MAX):
                citation, error = self._fetch_medline(
                    searched, start, self._MEDLINE_MAX
                )

                medline_data.extend(citation.values())
                error_pmids = {**error_pmids, **error}
                pbar.update(min(count, start + self._MEDLINE_MAX) - start)
        finally:
            pbar.close()

        if elink is True and count > 0:
            self._fetch_elink(medline_data, verbose=verbose)

        return searched, medline_data, error_pmids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def count_text(self, search_text, relday=0):
        """Conduct a text search but just return the search object with the
        count of matches.

        Parameters
        ----------
        search_text : `str`
            The text search string.
        relday : `int`
            Relday is the timeframe that the search will look in so a relday
            of 365 is get the hits from the last year a relday of 0 just gets
            all hits irrespective of time frame.

        Returns
        -------
        search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
            The search summary object that has a ``get_count`` method.

        Raises
        ------
        IndexError
            If there are no search results.
        """
        return self._do_text_search(search_text, relday=relday)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _do_text_search(self, search_text, relday=0):
        """Conduct a text search.

        Parameters
        ----------
        search_text : `str`
            The text search string.
        relday : `int`
            Relday is the timeframe that the search will look in so a relday
            of 365 is get the hits from the last year a relday of 0 just gets
            all hits irrespective of time frame.

        Returns
        -------
        search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
            The search summary object.

        Raises
        ------
        IndexError
            If there are no search results.
        """
        search_results = Entrez.read(
            Entrez.esearch(
                db="pubmed",
                term=search_text,
                relday=relday,
                datetype="pdat",
                usehistory="y"
            )
        )
        # if there are results
        if search_results:
            # If we are verbose then message out the search statistcs
            searched = PubmedSearchSummary(
                search_text, search_results
            )
        else:
            raise IndexError("no search results")
        return searched

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_pmids(self, pmids, elink=False, verbose=False):
        """Conduct a pubmed ID search.

        Parameters
        ----------
        search_pubmed_ids : `list` of `str`
             Pubmed IDs to fetch. Note that although the IDs are numeric they
             are handled as strings.
        elink : `bool`, optional, default: `False`
            Download the e-link journal URLs as well as the search results.
        verbose : `bool`, optional, default: `False`
            Display a pubmed query progress bar for downloading citations and
            if ``elink`` is ``True``, for downloading eLinks.

        Returns
        -------
        search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
            The search summary object - note this will be the search summary
            of the last search performed as is of little practical use, it is
            only returned to maintain a similar interface to the text search.
        medline_data : `list` of `paper_scraper.pubmed.MedlineRecord`
            The result medline entries.
        error_pmids : `dict`
            A log of errors associated with entries, the keys are PMIDs the
            values are the errors.

        Raises
        ------
        IndexError
            If there are no search results.
        """
        medline_data = list()
        error_pmids = dict()
        count = len(pmids)

        kwargs = dict(
            desc="[info] downloading PMIDs",
            unit=" PMIDs",
            disable=not verbose,
            total=count,
            leave=False
        )
        pbar = tqdm(**kwargs)
        try:
            for start in range(0, count, self._ELINK_MAX):
                pmid_subset = pmids[start:start+self._ELINK_MAX]
                if len(pmid_subset) > 0:
                    search_id = ",".join(pmid_subset)
                    # The pubmed IDs have to be strings joined on ,
                    post_handle = Entrez.epost(
                            "pubmed",
                            id=search_id
                        )
                    search_results = Entrez.read(post_handle)
                    post_handle.close()

                    # if there are results
                    if search_results:
                        # If we are verbose then message out the search
                        # statistcs
                        search_summary = PubmedSearchSummary(
                            search_id, search_results
                        )
                    else:
                        raise IndexError("no search results")

                    citation, error = self._fetch_medline(
                        search_summary, 0, self._ELINK_MAX
                    )
                    medline_data.extend(citation.values())
                    error_pmids = {**error_pmids, **error}
                pbar.update(len(pmid_subset))
        finally:
            pbar.close()

        # Error if we find nothing
        if len(medline_data) == 0:
            raise IndexError("no PMIDs found")

        if elink is True:
            self._fetch_elink(medline_data, verbose=verbose)

        return search_summary, medline_data, error_pmids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_medline(self, search, start, count):
        """Fetch the medline data for a subset of results.

        Parameters
        ----------
        search : `paper_scraper.pubmed.PubmedSearchSummary`
            The search summary object that has ``get_web_env`` and
            ``get_query_key``methods.
        start : `int`
            The start position of the download (in the whole batch).
        count : `int`
            The expected number of entries in the download.

        Returns
        -------
        medline_data : `list` of `paper_scraper.pubmed.MedlineRecord`
            The result medline entries.
        error_pmids : `dict`
            A log of errors associated with entries, the keys are PMIDs the
            values are the errors.
        """
        # Slow down the queries
        time.sleep(1)

        # Generate the key for the cache. Not ethat in order for the cache to
        # be used the query must be the same the total number of results in the
        # query must be the same and the position and number of results being
        # gathered must also be the same. These are likely to be the case when
        # the query is run in very close temporal proximity - ie. re-runs after
        # failures.
        cache_key = self.cache.get_key(
            (search.user_search, search.get_count(), start, count)
        )
        cache_entry = self.cache.get_from_cache(cache_key)

        # If we have a cached query subset then we return it, if not we use the
        # results the the issued query
        if cache_entry is not None:
            # print("using cache")
            return cache_entry[0], cache_entry[1]

        medline_data = dict()
        error_pmids = dict()

        # Fetch a batch of data
        try:
            fetch_handle = Entrez.efetch(db="pubmed",
                                         rettype="medline",
                                         retmode="text",
                                         retstart=start,
                                         retmax=count,
                                         webenv=search.get_web_env(),
                                         query_key=search.get_query_key())
        except Exception:
            pp.pprint(search.search)
            raise

        # Get the data and parse it back out as a list
        data = list(Medline.parse(fetch_handle))

        # close the handle
        fetch_handle.close()

        # Store the data internally
        for i in data:
            try:
                # Attempt to set the record via the PMID, this will fail with
                # a KeyError if the PMID does not exist (maybe it was not a
                # True PMID?)
                medline_data[i['PMID']] = MedlineRecord(**i)
            except KeyError:
                # How to get the error PMIDs?
                pass
                # error_pmids[i['PMID']] = e

        self.cache.add_to_cache(
            cache_key, (medline_data, error_pmids)
        )

        return medline_data, error_pmids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_elink(self, medline_data, verbose=False):
        """Fetch the elink data for a batch of medline citation data.

        Parameters
        ----------
        medline_data : `list` of `paper_scraper.pubmed.MedlineRecord`
            The result medline entries.
        verbose : `bool`, optional, default: `False`
            Display a eLink query progress bar.

        Returns
        -------
        medline_data : `list` of `paper_scraper.pubmed.MedlineRecord`
            The result medline entries. This has a ``GN`` tag added with
            keywords: PROVIDER: <journal_provider> and URL: <download URL>

        Notes
        -----
        Downloading eLinks is temperamental and prone to failure for no
        apparent reason. So this method is designed not to fail but rather skip
        and bad queries and report.
        """
        # Slow down the queries
        time.sleep(1)

        # Will hold the journal provider/hyperlink for each returned pubmed
        # ID. If a PMID does not have this info then the value will be None
        elink_data = dict()

        # Will hold the PMIDs that we want to search for
        all_pmids = []
        for i in medline_data:
            try:
                pmid = self.check_pubmed_id(i.get_data_for('PMID'))
                data = self.cache.get_from_cache(pmid)
                if data is not None:
                    elink_data[pmid] = data
                else:
                    all_pmids.append(pmid)
            except ValueError:
                pass

        kwargs = dict(
            desc="[info] downloading elinks",
            unit=" elinks",
            disable=not verbose,
            total=len(all_pmids) + len(elink_data),
            leave=False,
            initial=len(elink_data)
        )
        pbar = tqdm(**kwargs)
        try:
            for start in range(0, len(all_pmids), self._ELINK_MAX):
                pmids = all_pmids[start:start+self._ELINK_MAX]
                match_ids = pmids.copy()
                if len(pmids) > 0:
                    search_id = ",".join(pmids)

                    # The pubmed IDs have to be strings joined on ,
                    post_handle = Entrez.epost(
                            "pubmed",
                            id=search_id
                        )
                    search_results = Entrez.read(post_handle)
                    post_handle.close()

                    # if there are results
                    if search_results:
                        # If we are verbose then message out the search
                        # statistcs
                        search_summary = PubmedSearchSummary(
                            search_id, search_results
                        )
                    else:
                        raise IndexError("no search results")

                    retries = 5
                    for i in range(retries):
                        try:
                            handle = Entrez.elink(
                                    dbfrom="pubmed",
                                    cmd="prlinks",
                                    webenv=search_summary.get_web_env(),
                                    query_key=search_summary.get_query_key()
                                )
                            data = Entrez.read(handle)
                            handle.close()
                            break
                        except Entrez.Parser.CorruptedXMLError:
                            if i == retries - 1:
                                pp.pprint(handle)
                                handle.close()
                                raise
                        except Exception as e:
                            if i == retries - 1:
                                pp.pprint(handle)
                                handle.close()
                                print(
                                    "Error on elink for PMIDs: {0}".format(
                                        e.args[0]
                                    ),
                                    file=sys.stderr
                                )
                                for c, p in enumerate(pmids, 1):
                                    match_ids.pop(match_ids.index(pmid))
                                    print(f"{c}: {p}", file=sys.stderr)

                    # only do this if we have some data
                    if data is not None and len(data) > 0:
                        for i in data[0]['IdUrlList']['IdUrlSet']:
                            if i['ObjUrl']:
                                url = i['ObjUrl'][0]['Url'].strip()
                                pmid = i['Id']
                                provider = i['ObjUrl'][0]['Provider']['Name']
                                provider = provider.lower()
                                provider = re.sub(r"\s+", "_", provider)
                                elink_data[pmid] = (provider, url)
                                match_ids.pop(match_ids.index(pmid))
                                self.cache.add_to_cache(pmid, (provider, url))
                    else:
                        for i in match_ids:
                            elink_data[pmid] = None
                            self.cache.add_to_cache(pmid, None)
                    pbar.update(len(pmids))
        finally:
            pbar.close()

        for i in medline_data:
            try:
                provider, url = elink_data[i.get_data_for('PMID')]
                i.add_data("GN", f"PROVIDER: {provider}")
                i.add_data("GN", f"URL: {url}")
            except (TypeError, KeyError):
                pass

        return medline_data


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PubmedSearchSummary(object):
    """A container for a pubmed search, this just provides structured
    interaction with the search result.

    Parameters
    ----------
    search : `dict`
        The pubmed search results from the Entrez.read command.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, user_search, search):
        if not isinstance(search, Entrez.Parser.DictionaryElement):
            raise TypeError(
                "search results should be a "
                "Bio.Entrez.Parser.DictionaryElement"
            )
        self.user_search = user_search
        # Store them
        self.search = search

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_count(self):
        """Return the number of results.

        Returns
        -------
        count : `int`
            The number of returned citations.
        """
        try:
            return int(self.search['Count'])
        except KeyError:
            # This probably means there is an PMID ID search
            return 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_term(self):
        """Return the actual search term used to search pubmed (what pubmed
        searches).

        Returns
        -------
        search_term : `str`
            The pubmed translated search term.
        """
        try:
            return self.search['QueryTranslation']
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_quoted_phrase_not_found(self):
        """These are the phases that are 'in quotes' that are not found by the
        pubmed search.

        Returns
        -------
        not_found : `str`
            Any quoted phrases in the search that were not found.
        """
        try:
            return self.search['WarningList']['QuotedPhraseNotFound']
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phrase_not_found(self):
        """These are all the phrases that are that are not found by the pubmed
        search.

        Returns
        -------
        not_found : `str`
            Any phrases in the search that were not found.
        """
        try:
            return self.search['ErrorList']['PhraseNotFound']
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_phrase_ignored(self):
        """These are the phrases ignored by pubmed.

        Returns
        -------
        not_found : `str`
            Any phrases in the search that were ignored.
        """
        try:
            return self.search['WarningList']['PhraseIgnored']
        except KeyError:
            return ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_web_env(self):
        """This allows the retrival of a search.

        Returns
        -------
        web_env : `str`
            The web environment key that can be used to extract the actual
            results associated with the search.
        """
        try:
            return self.search['WebEnv']
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_query_key(self):
        """The query key is also used for search retrieval.

        Returns
        -------
        query_key : `str`
            The web query key that can be used to extract the actual
            results associated with the search.
        """
        try:
            return self.search['QueryKey']
        except KeyError:
            return None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MedlineRecord(object):
    """Represents the data in a pubmed record.

    Parameters
    ----------
    **tags
        tag keys and their data.

    Examples
    --------

    Imports from biopython and pubmed

    >>> from Bio import Entrez, Medline
    >>> from paper_scraper import pubmed

    Upload some pubmed IDs to the query server

    >>> post_handle = Entrez.epost("pubmed", id=",".join(["1328411", "1309370"]))

    Get the results and store in a search summary object

    >>> search_results = Entrez.read(post_handle)
    >>> post_handle.close()
    >>> search_summary = pubmed.PubmedSearchSummary(search_results)

    Fetch a batch of data from the query

    >>> fetch_handle = Entrez.efetch(db="pubmed",
    ...                              rettype="medline",
    ...                              retmode="text",
    ...                              retstart=start,
    ...                              retmax=count,
    ...                              webenv=search_summary.get_web_env(),
    ...                              query_key=search_summary.get_query_key())
    >>> fetch_handle.close()

    Get the data and parse it into MedineRecord objects

    >>> for i in list(Medline.parse(fetch_handle))
    ...     record = pubmed.MedlineRecord(**i)
    >>>
    """
    _TAGS = [('PMID', 'PubMed Unique Identifier'),
             ('OWN', 'Owner'),
             ('STAT', 'Status'),
             ('DA', 'Date Created'),
             ('DCOM', 'Date Completed'),
             ('LR', 'Date Last Revised'),
             ('IS', 'ISSN'),
             ('VI', 'Volume'),
             ('IP', 'Issue'),
             ('DP', 'Date of Publication'),
             ('TI', 'Title'),
             ('PG', 'Pagination'),
             ('LID', 'Location Identifier'),
             ('AB', 'Abstract'),
             ('CI', 'Copyright Information'),
             ('AD', 'Affiliation', list),
             ('FAU', 'Full Author', list),
             ('AU', 'Author', list),
             ('CN', 'Corporate Author', list),
             ('LA', 'Language', list),
             ('SI', 'Secondary Source ID', list),
             ('GR', 'Grant Number', list),
             ('PT', 'Publication Type', list),
             ('DEP', 'Date of Electronic Publication', list),
             ('PL', 'Place of Publication', list),
             ('TA', 'Journal Title Abbreviation', list),
             ('JT', 'Journal Title', list),
             ('JID', 'NLM Unique ID', list),
             ('CON', 'Comment on', list),
             ('RN', 'Registry Number/EC Number', list),
             ('SB', 'Subset', list),
             ('CIN', 'Comment in', list),
             ('MH', 'MeSH Terms', list),
             ('EIN', 'Erratum in', list),
             ('PMC', 'PubMed Central Identifier', list),
             ('EDAT', 'Entrez Date', list),
             ('MID', 'Manuscript Identifier', list),
             ('OID', 'Other ID', list),
             ('GN', 'General Note', list),
             ('MHDA', 'MeSH Date', list),
             ('IR', 'Investigator Name', list),
             ('CRDT', 'Do not Know', list),
             ('FIR', 'Full Investigator Name', list),
             ('PHST', 'Publication History Status', list),
             ('PMCR', 'Do not know', list),
             ('AID', 'Article Identifier', list),
             ('IRAD', 'Investigator Affiliation', list),
             ('PST', 'Publication Status', list),
             ('GS', 'Gene Symbol', list),
             ('RF', 'Number of References', str),
             ('OAB', 'Other Abstract', list),
             ('OCI', 'Other Copyright Information', list),
             ('OT', 'Other Term', list),
             ('OTO', 'Other Term Owner', list),
             ('PS', 'Personal Name as Subject', list),
             ('FPS', 'Full Personal Name as Subject', list),
             ('PUBM', 'Publishing Model', list),
             ('NM', 'Substance Name', list),
             ('ORI', 'Original report in', list),
             ('SFM', 'Space Flight Mission', list),
             ('TT', ' Transliterated Title', list),
             ('EFR', 'Erratum for', list),
             ('CRI', 'Corrected and Republished in', list),
             ('CRF', 'Corrected and Republished from', list),
             ('PRIN', 'Partial retraction in', list),
             ('PROF', 'Partial retraction of', list),
             ('RPI', 'Republished in', list),
             ('RPF', 'Republished from', list),
             ('RIN', 'Retraction in', list),
             ('ROF', 'Retraction of', list),
             ('UIN', 'Update in', list),
             ('UOF', 'Update of', list),
             ('SPIN', 'Summary for patients in', list),
             ('SO', 'Source', str)]
    """Tag information and expected value, each tuple is tag abbreviation,
    tag description and data type (`list or `int` or `str) (`list` of `tuple`)
    """
    _TAG_SET = set([i[0] for i in _TAGS])
    """A set of the medline TAGs (`set`)
    """
    _TAG_DESC = dict([(i[0], i[1]) for i in _TAGS])
    """A dictionary of TAGs with the keys being the medline TAG and the values
    being the TAG descriptions (`dict`)
    """
    _TAG_ORDER = dict([(i[0], c) for c, i in enumerate(_TAGS)])
    """The medline TAG order, a dictionary with the keys being the medline TAG
    and the values being the index position of the TAG (`dict`)
    """
    WRAP_WIDTH = 80
    """The line length for the medline text
    """
    _MAIN_FIELDS = [('pubmed_id', 'PMID', 'get_data_for'),
                    ('pubmed_central_id', 'PMC', 'get_data_for'),
                    ('pub_year', None, 'get_pub_year'),
                    ('pub_month', None, 'get_pub_month'),
                    ('title', 'TI', 'get_data_for'),
                    ('abstract', 'AB', 'get_data_for'),
                    ('first_author', None, 'get_first_author'),
                    ('last_author', None, 'get_last_author'),
                    ('journal_title', 'JT', 'get_data_for'),
                    ('journal_provider', None, 'get_journal_provider'),
                    ('download_url', None, 'get_download_url'),
                    ('doi', None, 'get_doi'),
                    ('summary_string', 'SO', 'get_data_for'),
                    ('mesh_terms', None, 'get_mesh_terms')]
    """The main field of interest in the medline record, this is just for
    convenience and can be used to subset the medline record, each tuple is
    field name, abbreviation (if present), extraction method name
    (list of tuple)
    """
    # Build a header list
    MAIN_FIELD_HEADER = [i[0] for i in _MAIN_FIELDS]
    """Column headings for the main fields (`list` of `str`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, **tagdict):
        self.tag_values = {}
        if tagdict:
            self.tag_values = tagdict

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __str__(self):
        """Call tor str(obj)
        """
        medstr = []
        # Loop through the medline tags in order
        for i in MedlineRecord._TAGS:
             # Get the ID of the tag
            t = i[0]

            try:
                formatted = []
                # If it is not a list entry
                if isinstance(self.tag_values[t], list) is False:
                    # formatted is an array of lines (or ossible a single line
                    formatted = self._wrap_and_format(t, self.tag_values[t])
                    medstr.extend(formatted)
                else:
                    for j in self.tag_values[t]:
                        formatted = self._wrap_and_format(t, j)
                        medstr.extend(formatted)
            except KeyError:
                pass

        # This will return a leading blank space the record
        # and then a newline at the end
        return "\n".join(medstr)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        return self.__str__()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _wrap_and_format(self, tag, string):
        """Controls the word wrapping

        Parameters
        ----------
        tag : `str`
            The tag represented by the string
        string : `str`
            The string to wrap (if needed)

        Returns
        -------
        formatted : `list` of `str`
            A list containing medline formatted strings for the tag.
        """
        # create a copy of the string
        s = string
        formatted = textwrap.wrap(s, width=self.__class__._WRAP_WIDTH)

        # Formatted is a list of arapped strings, now we want to add an indent
        # and also the tag on the first line
        for i, l in enumerate(formatted):
            if i > 0:
                formatted[i] = "%-6s%s" % (" ", l)
            else:
                formatted[i] = "%-4s- %s" % (tag, l)
        return formatted

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_main_fields(self):
        """A helper method to process the main fields of a MEDLINE record into a
        `list` that can be written to a file.

        Returns
        -------
        main_field_data : `dict`
            A summary dictionary of the main fields.
        """
        record = {}
        for i in self.__class__._MAIN_FIELDS:
            method = getattr(self, i[2])
            record[i[0]] = method(i[1])
        return record

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def is_tag_allowed(self, tag):
        """Determines if a tag is allowed in a medline record.

        Parameters
        ----------
        tag : `str`
            The MEDLINE tag

        Returns
        -------
        allowed : `bool`
            ``True`` if the tag is allowed ``False`` if not.
        """
        return tag in self.__class__._TAG_SET

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_description(self, tag):
        """Returns the meaning of a tag.

        Parameters
        ----------
        tag : `str`
            The MEDLINE tag.

        Returns
        -------
        description : `str`
            The full description of the tag.
        """
        if tag in self.__class__._TAG_SET:
            return tag in self.__class__._TAG_DESC[tag]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def tag_order(self, tag):
        """Returns the order of a tag in the medline record.

        Parameters
        ----------
        tag : `str`
            The MEDLINE tag.

        Returns
        -------
        order : `int`
            The index of the tag in a medline record.
        """
        if tag in self.__class__._TAG_SET:
            return tag in self.__class__._TAG_ORDER[tag]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_data(self, tag, data):
        """
        Adds data to a tag. If the tag is not a list then the data gets
        over-written, if it is a list then the data gets appended

        Parameters
        ----------
        tag : `str`
            The medline tag. This is an uppercase string.
        data : `Any`
            The data to add to the tag.
        """
        if tag in self.__class__._TAG_SET:
            try:
                self.tag_values[tag]
            except KeyError:
                # The tag is not represented, so we initialise as a list
                self.tag_values[tag] = []

            # Now we adjust the data
            if isinstance(self.tag_values[tag], list) is False:
                self.tag_values[tag] = data
            else:
                self.tag_values[tag].append(data)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_data_for(self, tag):
        """Return the data for a specific TAG.

        Parameters
        ----------
        tag : `str`
            The medline tag. This is an uppercase string.

        Returns
        -------
        data : `Any`
            The data associated with the tag, could be a ``str``, ``int`` or
            ``list``.
        """
        try:
            return self.tag_values[tag].strip()
        except KeyError:
            if tag in self.__class__._TAG_SET:
                return None
            raise KeyError("unknown tag '{0}'".format(tag))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_pub_year(self, *args):
        """Return the publication year.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        pub_year : `int`
            The publication month or ``NoneType`` if there is no publication
            year.
        """
        try:
            dp = self.tag_values['DP'].strip()
            dates = re.split(r'\s+', dp)
            return int(dates[0])
        except (KeyError, ValueError):
            # ValueError: invalid literal for int() with base 10: 'Winter'
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_pub_month(self, *args):
        """Return the publication month.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        pub_month : `str`
            The publication month or ``NoneType`` if there is no publication
            month.
        """
        try:
            dp = self.tag_values['DP'].strip()
            dates = re.split(r'\s+', dp)
            return dates[1]
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_first_author(self, *args):
        """Return the first author.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        first_author : `str`
            The last author or ``NoneType`` if there is no first author.
        """
        try:
            au = self.tag_values['AU'][0]
            return au.strip()
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_last_author(self, *args):
        """Return the last author.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        last_author : `str`
            The last author or ``NoneType`` if there is no last author.
        """
        try:
            au = self.tag_values['AU'][-1]
            return au.strip()
        except (KeyError, IndexError):
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_journal_provider(self, *args):
        """Return the journal provider.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        provider : `str`
            The journal provider or ``NoneType`` if there is no journal
            provider.
        """
        test = r'PROVIDER:\s+'

        try:
            gn = self.tag_values['GN']

            for i in gn:
                if re.match(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_download_url(self, *args):
        """Return the download url.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        download_url : `str`
            The primary elink URL.
        """
        test = r'URL:\s+'

        try:
            gn = self.tag_values['GN']

            for i in gn:
                if re.match(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_doi(self, *args):
        """Return the DOI

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        doi : `str`
            The doi or ``NoneType`` if there is no doi.
        """
        test = r'\s*\[doi\]\s*$'

        try:
            lid = self.tag_values['LID'].strip()

            if re.search(test, lid):
                return re.sub(test, '', lid)

            aid = self.tag_values['AID']
            for i in aid:
                if re.search(test, i):
                    return re.sub(test, '', i).strip()
            return None
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_mesh_terms(self, *args):
        """Return the mesh terms.

        Parameters
        ----------
        *args
            Ignored, for interface only.

        Returns
        -------
        mesh_terms : `list` of `tuple`
            The mesh terms associated with the citation. Each tuple has the
            structure: key_term (first term), modifier terms. An empty list is
            returned if no MeSH IDs are found (`list` of `str`).
        """
        all_mesh = []
        try:
            mh = self.tag_values['MH']

            for i in mh:
                major = False

                # There may be modifiers
                sub_mh = i.split(r'/')

                # Now search for a major modifier
                for c, i in enumerate(sub_mh):
                    if i.startswith("*"):
                        major = True
                        sub_mh[c] = i[1:]

                key_term = sub_mh.pop(0)
                all_mesh.append((key_term, sub_mh, major))
            return all_mesh
        except (IndexError, KeyError):
            return []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _main_search():
    """The entry point for the pubmed text search script. For API use see
    notes.

    See also
    --------
    paper_scraper.pubmed.count_pubmed
    paper_scraper.pubmed.search_pubmed
    paper_scraper.pubmed.PubmedQuery
    """
    # Initialise and parse the command line arguments
    parser = _init_search_cmd_args()
    args = _parse_cmd_args(parser)

    run_func = search_pubmed
    if args.counts_only is True:
        run_func = count_pubmed

    _main(_SEARCH_SCRIPT_NAME, run_func, args.search_terms, args)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _main_pmids():
    """The entry point for the PMID search script. For API use see
    notes.

    See also
    --------
    paper_scraper.pubmed.get_pubmed_ids
    paper_scraper.pubmed.PubmedQuery
    """
    # Initialise and parse the command line arguments
    parser = _init_pmid_cmd_args()
    args = _parse_cmd_args(parser)
    _main(_PMID_SCRIPT_NAME, get_pubmed_ids, args.search_pmids, args)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _main(script_name, run_func, search, args):
    """The main entry point for the script. For API use see notes.

    Parameters
    ----------
    script_name : `str`
        The name of the script being run.
    run_func : `function`
        The function that will be called to actually run the required code.
    search : `list` of `str`
        Either search terms, search PMIDs or a file path containing these.
    args : `argparse.namespace`
        The result from calling argparse.parser.parse_args()

    See also
    --------
    paper_scraper.pubmed.count_pubmed
    paper_scraper.pubmed.search_pubmed
    paper_scraper.pubmed.get_pubmed_ids
    paper_scraper.pubmed.PubmedQuery
    """
    logger = log.init_logger(
        script_name, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        search_terms = []
        with open(search[0], 'rt') as search_file:
            for row in search_file:
                search_terms.append(row.strip())
    except FileNotFoundError:
        # Must be text
        search_terms = search

    # Log some specifics
    logger.info(f"search entity #: {len(search_terms)}")
    logger.info(f"search with: {run_func.__name__}")

    verbose = False
    if args.verbose > 1:
        verbose = True

    try:
        run_func(
            search_terms, args.email, args.citation_counts, args.citation_info,
            verbose=verbose, elink=args.elink, relday=args.relday
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_info_writer(outfile, **kwargs):
    """Get the file object and writer to the citation info file.

    Parameters
    ----------
    outfile : `str`
        The output path to the citation information file.
    **kwargs
        Keyword arguments to `csv`

    Returns
    -------
    file_obj : ``
        The output file object.
    writer : `csv.DictWriter`
        The writer object.

    Notes
    -----
    This also writes the header of the file.
    """
    file_obj = open(outfile, 'wt')

    try:
        write_obj = csv.DictWriter(
            file_obj, ['user_term'] + MedlineRecord.MAIN_FIELD_HEADER,
            **kwargs
        )
        write_obj.writeheader()
    except Exception:
        file_obj.close()
        raise

    return file_obj, write_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_count_writer(outfile, **kwargs):
    """Get the file object and writer to the citation count file.

    Parameters
    ----------
    outfile : `str`
        The output path to the citation counts file.
    **kwargs
        Keyword arguments to `csv`

    Returns
    -------
    file_obj : ``
        The output file object.
    writer : `csv.DictWriter`
        The writer object.

    Notes
    -----
    This also writes the header of the file.
    """
    file_obj = open(outfile, 'wt')

    try:
        write_obj = csv.writer(
            file_obj, **kwargs
        )
        write_obj.writerow(
            ['user_term', 'pubmed_term', 'ncitations',
             'quoted_phrase_not_found', 'phrase_not_found',
             'phrase_ignored']
        )
    except Exception:
        file_obj.close()
        raise

    return file_obj, write_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_delimiters(delimiter, sub_delimiter):
    """Make sure that the delimiter and the sub-delimiter are not the same.

    Parameters
    ----------
    delimiter : `str`
        The value to use for the delimiter in the output row.
    sub_delimiter : `str`
        The value to use for nested sub delimiters in the output row.

    Raises
    ------
    ValueError
        If the delimiter and sub-delimiter are the same.
    """
    if delimiter == sub_delimiter:
        raise ValueError("delimiter and sub-delimiter are identical")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_count_row(user_search, search_summary, sub_delimiter="|"):
    """Get an output row data for the citation count.

    Parameters
    ----------
    user_search : `str`
        The search string that was given to pubmed (not the pubmed translation
        of it).
    search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
        The search summary object to extract from.
    sub_delimiter : `str`, optional, default: `|`
        The value to use for nested sub delimiters in the output row.

    Returns
    -------
    outrow : `list`
        A list that can be used with `csv.writer`
    """
    return [
        user_search, search_summary.get_term(), search_summary.get_count(),
        none_join(
            search_summary.get_quoted_phrase_not_found(), sub_delimiter
        ),
        none_join(search_summary.get_phrase_not_found(), sub_delimiter),
        none_join(search_summary.get_phrase_ignored(), sub_delimiter)
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_citation_info_row(user_search, search_summary, citation,
                           sub_delimiter="|"):
    """Get an output row data for the citation information.

    Parameters
    ----------
    user_search : `str`
        The search string that was given to pubmed (not the pubmed translation
        of it).
    search_summary : `paper_scraper.pubmed.PubmedSearchSummary`
        The search summary object to extract from.
    citation : `paper_scraper.pubmed.MedlineRecord`
        The medline citation.
    sub_delimiter : `str`, optional, default: `|`
        The value to use for nested sub delimiters in the output row.

    Returns
    -------
    outrow : `dict`
        A list that can be used with `csv.DictWriter`
    """
    mt = []
    for term, modifiers, is_pref in citation.get_mesh_terms():
        modifiers = "/".join(modifiers)
        mt.append("[{0}|{1}|{2}]".format(term, modifiers, int(is_pref)))

    outrow = citation.get_main_fields()
    outrow['mesh_terms'] = "".join(mt)
    outrow['user_term'] = user_search
    return outrow


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def none_join(field, delimiter):
    """Join ``field`` on ``delimiter`` but handle situations were field is
    ``NoneType`` or contains ``int``.

    Parameters
    ----------
    field : `list` or `NoneType`
        The field to join on delimiter.
    delimiter : `str`
        The delimiter to join field on.

    Returns
    -------
    joined_field : `str`
        The joined field, ``NoneType`` is converted to "".
    """
    try:
        return delimiter.join(field)
    except TypeError:
        if field is None:
            return ""
        else:
            delimiter.join([str(i) for i in field])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def count_pubmed(search_terms, email, citation_counts_file, *args,
                 verbose=False, relday=0, delimiter="\t", sub_delimiter="|",
                 **kwargs):
    """Perform a text search against pubmed and store the citation counts only.

    This is a utility function for use with the CMD-line, although can be used
    in an API call. However, for full control, see
    `paper_scraper.pubmed.PubmedQuery`.

    Parameters
    ----------
    search_terms : `list` of `str`
        The text searches. These should be formatted in the same way you would
        on the pubmed website.
    email : `str`
        The email address of the user performing the search. This is so pubmed
        can contact users performing excessive searches rather than simply
        blacklisting their IP.
    citation_counts_file : `str`
        The file name to write the citation counts to.
    *args
        Ignored
    verbose : `bool`, optional, default: `False`
        Provide a progress update.
    relday : `int`, optional, default: `0`
        The relative day count window in which to perform the search.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the output file.
    sub_delimiter : `str`, optional, default: `|`
        The delimiter to use for nested delimited sub-fields in the output
        file.
    **kwargs
        Ignored

    See also
    --------
    paper_scraper.pubmed.PubmedQuery
    """
    # Make sure we have distinct delimiters and sub delimiters
    check_delimiters(delimiter, sub_delimiter)

    q = PubmedQuery(email)

    cc, ccw = _get_citation_count_writer(
        citation_counts_file, delimiter=delimiter
    )

    try:
        for i in tqdm(search_terms, disable=not verbose, unit=" search terms",
                      desc="[info] searching pubmed"):
            search_summary = q.count_text(i, relday=relday)
            ccw.writerow(_get_citation_count_row(
                i, search_summary, sub_delimiter=sub_delimiter
            ))
    finally:
        cc.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def search_pubmed(search_terms, email, citation_counts_file,
                  citation_info_file, verbose=False, elink=True, relday=0,
                  delimiter="\t", sub_delimiter="|"):
    """Perform a text search against pubmed and store the citation info and
    counts.

    This is a utility function for use with the CMD-line, although can be used
    in an API call. However, for full control, see
    `paper_scraper.pubmed.PubmedQuery`.

    Parameters
    ----------
    search_terms : `list` of `str`
        The text searches. These should be formatted in the same way you would
        on the pubmed website.
    email : `str`
        The email address of the user performing the search. This is so pubmed
        can contact users performing excessive searches rather than simply
        blacklisting their IP.
    citation_counts_file : `str`
        The file name to write the citation counts to.
    citation_info_file : `str`
        The file name to write the citation information to.
    verbose : `bool`, optional, default: `False`
        Provide a progress update.
    elink : `bool`, optional, default: `False`
        Gather eLink journal provider/download URL info. This will slow the
        query down.
    relday : `int`, optional, default: `0`
        The relative day count window in which to perform the search.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the output file.
    sub_delimiter : `str`, optional, default: `|`
        The delimiter to use for nested delimited sub-fields in the output
        file.

    See also
    --------
    paper_scraper.pubmed.PubmedQuery
    """
    # Make sure we have distinct delimiters and sub delimiters
    check_delimiters(delimiter, sub_delimiter)

    q = PubmedQuery(email)

    ci, ciw = _get_citation_info_writer(
        citation_info_file, delimiter=delimiter
    )
    cc, ccw = _get_citation_count_writer(
        citation_counts_file, delimiter=delimiter
    )

    try:
        for i in tqdm(search_terms, disable=not verbose, unit=" search terms",
                      desc="[info] searching pubmed"):
            search_summary, citations, errors = q.search_text(
                i, elink=elink, relday=relday, verbose=verbose
            )
            for c in citations:
                ciw.writerow(_get_citation_info_row(
                    i, search_summary, c, sub_delimiter=sub_delimiter
                ))

            ccw.writerow(_get_citation_count_row(
                i, search_summary, sub_delimiter=sub_delimiter
            ))
    finally:
        ci.close()
        cc.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_pubmed_ids(search_pmids, email, citation_counts_file,
                   citation_info_file, verbose=False, elink=True,
                   delimiter="\t", sub_delimiter="|"):
    """Perform a search for pubmed IDs (PMIDs) against pubmed and store the
    citation info and counts.

    This is a utility function for use with the CMD-line, although can be used
    in an API call. However, for full control, see
    `paper_scraper.pubmed.PubmedQuery`.

    Parameters
    ----------
    search_pmids : `list` of `str`
        The PMIDs searches. Note that although PMIDs are integers they are
        handled as strings.
    email : `str`
        The email address of the user performing the search. This is so pubmed
        can contact users performing excessive searches rather than simply
        blacklisting their IP.
    citation_counts_file : `str`
        The file name to write the citation counts to.
    citation_info_file : `str`
        The file name to write the citation information to.
    verbose : `bool`, optional, default: `False`
        Provide a progress update.
    elink : `bool`, optional, default: `False`
        Gather eLink journal provider/download URL info. This will slow the
        query down.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the output file.
    sub_delimiter : `str`, optional, default: `|`
        The delimiter to use for nested delimited sub-fields in the output
        file.

    See also
    --------
    paper_scraper.pubmed.PubmedQuery
    """
    # Make sure we have distinct delimiters and sub delimiters
    check_delimiters(delimiter, sub_delimiter)

    q = PubmedQuery(email)

    ci, ciw = _get_citation_info_writer(
        citation_info_file, delimiter=delimiter
    )
    cc, ccw = _get_citation_count_writer(
        citation_counts_file, delimiter=delimiter
    )

    try:
        for i in tqdm(search_pmids, disable=not verbose, unit="search terms",
                      desc="[info] searching pubmed"):
            search_summary, citations, errors = q.search_pmids(
                i, elink=elink, verbose=verbose
            )
            for c in citations:
                ciw.writerow(_get_citation_info_row(
                    i, search_summary, c, sub_delimiter=sub_delimiter
                ))

            ccw.writerow(_get_citation_count_row(
                i, search_summary, sub_delimiter=sub_delimiter
            ))
    finally:
        ci.close()
        cc.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_search_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Search pubmed using a search text."
    )

    parser.add_argument(
        'search_terms',
        type=str,
        nargs="+",
        help="One or more search terms, or a file containing search terms"
        " 1-per line"
    )
    parser.add_argument('-o', '--counts-only',  action="store_true",
                        help="Do not download citations, just get the citation"
                        " counts")

    # Now add  the common args
    _init_common_cmd_args(parser)

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_pmid_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Search pubmed using pubmed identifiers."
    )

    parser.add_argument(
        'search_pmids',
        type=str,
        nargs="+",
        help="Search pubmed identifiers or a file containing search IDs 1-per"
        " line"
    )

    # Now add  the common args
    _init_common_cmd_args(parser)

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_common_cmd_args(parser):
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser.add_argument('-e', '--email',
                        type=str,
                        help="A value (treated as a string) to multiply")
    parser.add_argument('-I', '--citation-info',
                        type=str, default="citation_info.txt",
                        help="A file to write all the citation info to")
    parser.add_argument('-C', '--citation-counts',
                        type=str, default="citation_counts.txt",
                        help="A file to write all the citation counts to")
    parser.add_argument('-r', '--relday',
                        type=int, default=0,
                        help="The relative days back to perform the search")
    parser.add_argument('-E', '--elink',  action="store_true",
                        help="Download the eLink journal provider and URLs,"
                        " this slows the queries down but you have a direct"
                        " hyperlink to the publication")
    parser.add_argument('-v', '--verbose',  action="count",
                        help="give more output, use -vv to turn on progress "
                        "monitoring")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    _main_search()
