"""Download publication data from journal websites using pubmed IDs. This is
currently a work in progress and does not do anything other than provide the
command line interface.
"""
# Importing the version number (in __init__.py) and the package name
from paper_scraper import (
    __version__,
    __name__ as pkg_name,
    log
)
import argparse
import sys
import os
import logging
# import pprint as pp

_SCRIPT_NAME = "paper-scrpaer"
"""The name of the script (`str`)
"""

# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ConfigManager(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        """
        pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _main():
    """The main entry point for the script. For API use see
    ``something else``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        run_paper_scraper(
            args.multiply_this, args.by_these, verbose=args.verbose
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_paper_scraper(*args, **kwargs):
    """The API entry point for running the command line program.

    This means that anyone wanting to run the program from their python code
    can call this function instead of _main(). This is also handy for calling
    with ``pytest``.

    Parameters
    ----------
    arg1 : `str`
        A string. This string will be multiplied by the length of the list
    arg2 : `list`
        A list. The list length will be used to multiply the string
    vervose : `bool`, optional, default: `False`
        Tell the user what I am doing
    """
    # The call to getLogger will return the same logger object if the name is
    # the same
    logger = logging.getLogger(_SCRIPT_NAME)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        "pmids", type=int, nargs="+",
        help="A number of pubmed IDs that need to be harvested, or a file"
        " containing pubmed IDs"
    )
    parser.add_argument(
        "-f", "--force", action="store_true",
        help="Do you want force re-downloading for previously retrieved"
        "records"
    )
    # parser.add_argument(
    #     "--debug", action="store_true",
    #     help="For debugging allow the execution of selenium commands to search"
    #     " for web elements"
    # )
    parser.add_argument(
        "--limit", type=int, default=-1,
        help="For debugging stop processing after ``--limit`` records"
    )
    # parser.add_argument(
    #     "--filter", type=str, nargs="+",
    #     help="Filters to determine what records will be processed, with the"
    #     " format TAG=VALUE, use PROV,URL for provider and url filters"
    # )
    # parser.add_argument(
    #     "--file", type=str,
    #     help="A file containing pubmed IDs that need to be harvested (one per"
    #     " line)"
    # )
    parser.add_argument(
        "--proxy", type=str,
        help="A SOCKS proxy to use, of the format <HOST>:<PORT>"
    )
    # parser.add_argument(
    #     "--log-prefix", type=str,
    #     default=os.path.join(os.getcwd(), ""),
    #     help="A file prefix for the log files"
    # )
    # parser.add_argument(
    #     "--writemedline", type=str,
    #     help="A file to write medline output for any PMIDs that have been"
    #     " searched"
    # )
    parser.add_argument(
        "--sleep", type=float, default=1,
        help="The time in seconds between commands"
    )
    parser.add_argument(
        "--email", type=str,
        help="The email address for the pubmed queries, if not provided this"
        " should be given in the config file"
    )
    # parser.add_argument(
    #     "--sqldb", type=str, default="sqlite:///DB/pdf_harvester.db",
    #     help="An SQL alchemby connection string to the background database"
    # )
    # parser.add_argument(
    #     "--medline-only", action="store_true",
    #     help="Do you want to exit after the medline query?"
    # )
    # parser.add_argument(
    #     "--no-search", action="store_true",
    #     help = "Do not search for anything"
    # )
    parser.add_argument(
        "--random", action="store_true",
        help="Process in a random order"
    )
    parser.add_argument(
        "-v", "--verbose",  action="store_true",
        help="give more output"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # TODO: You can perform checks on the arguments here if you want
    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    _main()
