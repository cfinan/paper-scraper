biopython
bump2version
lxml
pysocks
pytest>=5.*
pytest-dependency>=0.5*
selenium
tqdm
