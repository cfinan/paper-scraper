``paper_scraper`` package
=========================

``paper_scraper.pubmed`` module
-------------------------------

.. automodule:: paper_scraper.pubmed
   :members:
   :undoc-members:
   :show-inheritance:
