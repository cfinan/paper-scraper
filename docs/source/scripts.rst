====================
Command line scripts
====================

The command line scripts implemented in paper scraper are documented here.

Querying Pubmed
===============

There are a coupleof scripts to query pubmed programmatically, either by keyword or pubmed IDs.

.. _pubmed_text_search:

``pubmed-search``
-----------------

.. argparse::
   :module: paper_scraper.pubmed
   :func: _init_search_cmd_args
   :prog: pubmed-search

.. _pubmed_id_search:

``pubmed-id-query``
-------------------

.. argparse::
   :module: paper_scraper.pubmed
   :func: _init_pmid_cmd_args
   :prog: pubmed-id-query
