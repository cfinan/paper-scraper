.. paper-scraper documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to paper-scraper
========================

`paper-scraper <https://gitlab.com/cfinan/paper-scraper>`_ is a prototype scientific journal web-scraping application built on top of Selenium. It does not do that much at the moment, other than implementing a pubmed query API layer (and command-line program). There is some journal scraping code that will gradually be added from a previous iteration.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 4
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
