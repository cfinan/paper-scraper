# Getting Started with paper-scraper

__version__: `0.1.1a0`

paper-scraper is a prototype scientific journal web-scraping application built on top of Selenium. It does not do that much at the moment, other than implementing a pubmed query API layer (and command-line program). There is some journal scraping code that will gradually be added from a previous iteration.

There is [online](https://cfinan.gitlab.io/paper-scraper/index.html) documentation for paper-scraper.

## Installation instructions
At present, paper-scraper is undergoing development and no packages exist yet on PyPy or in Conda. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
git clone git@gitlab.com:cfinan/paper-scraper.git
cd paper-scraper
```

### Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./resources/conda/env`. A new conda environment called `paper_scraper_py39` can be built using the command:

```
# From the root of the paper-scraper repository
conda env create --file ./resources/conda/env/py39/conda_create.yml
```

To add to an existing environment use:
```
# From the root of the paper-scraper repository
conda env update --file ./resources/conda/env/py39/conda_update.yml
```

There are also Conda environments for Python v3.7, v3.8 and and v3.10. Then to install paper_scrapper you can either do:
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the paper-scraper repository. The difference with this is that you can just to a `git pull` to update paper_scrapper, or switch branches without re-installing:
```
python -m pip install -e .
```

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install paper_scrapper as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then to install paper_scrapper you can either do:
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the paper-scraper repository. The difference with this is that you can just to a `git pull` to update paper_scrapper, or switch branches without re-installing:
```
python -m pip install -e .
```

## Installing the webdrivers
Selanium also requires a [browser driver](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/) to be installed (webdriver). For Firefox this is referred to as [`geckodriver`](https://github.com/mozilla/geckodriver/releases) and for Chrome it is called [`chromedriver`](https://chromedriver.chromium.org/downloads).The driver's location will need to be in your PATH in your `~/.bashrc` or `~/.bash_profile` and should be matched to your browser version.

## Next steps...
After installation you will may to run the tests. If you have cloned the repository you can run the command below from the root of the repository. Please note, that there are currently no pytests. I hope to implement some when I can get headless mode running:

1. Run the tests using ``pytest ./tests``

## Command endpoints
There are also some command line endpoints that are installed along with the package. Documentation for these can be found [here](https://cfinan.gitlab.io/paper-scraper/scripts.html).

## Other projects
I thought I would have a look to see what is out there already. Below are some links, there may be some things but nothing seems to do exactly what I want. However, I will need to have a more in depth look.

* http://flowingmotion.jojordan.org/2010/06/02/how-to-download-100-pdf-files-from-a-website-in-one-batch/
* https://addons.mozilla.org/en-US/firefox/addon/downthemall/
* https://www.biostars.org/p/45647/
* https://www.biostars.org/p/2928/
* http://www.myexperiment.org/search?query=pubmed&type=all
* http://www.papertoolbox.com/
* https://github.com/elfar/PubmedPDF
* https://github.com/billgreenwald/Pubmed-Batch-Download
* http://bioinformatics.risha.me/tag/eutils/
* https://bytes.com/topic/visual-basic-net/answers/388086-automatically-download-save-pdf-files-website


